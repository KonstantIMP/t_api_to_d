/**
 * Functions for prettier code generation
 */
module kimp.markup;

import std.string, std.conv, std.ascii : isLower;

/**
 * Class with code prettier functions
 */
class Markup {
    /** 
     * Generate pretty name for property functions
     * Params:
     *   entryName = Name of the entry for generation properties
     * Returns: String with pretty name
     */
    public static string makePropName (string entryName) {
        string propName = entryName;

        while (propName.count('_')) {
            ulong i = propName.indexOf('_');
            propName = propName[0 .. i] ~ to!string(toUpper(propName[i + 1])) ~ propName[i + 2 .. $];
        }

        return propName;
    }

    /** 
     * Generate name for file with telegramtype
     * Params:
     *   typeName = Name of the type
     * Returns: Name for the file (and module)
     */
    public static string makeFileName (string typeName) {
        string result = "";

        typeName = to!(string)(toLower(typeName[0])) ~ typeName[1 .. $];

        foreach (s; typeName) {
            if (isLower(s)) result ~= s;
            else {
                result = result ~ "_" ~ to!string(toLower(s));
            }
        }

        return result;
    }
}
