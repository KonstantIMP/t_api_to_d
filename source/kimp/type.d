/**
 * Classes for containing data types
 */
module kimp.type;

import kimp.markup, std.string : count;
import std.ascii : isWhite;
import std.array : split;

/**
 * Contains data about type's entry or method's param
 */
class Entry {
    public string entryName;
    public string entryType;
    public string entryDesc;
    public bool entryIsOptional;

    /** 
     * Creates new entry object
     * Params:
     *   name = Name of the entry
     *   type = Type of the entry
     *   desc = Description of the entry
     *   optional = True if is not required
     */
    pure nothrow public this (string name, string type, string desc, bool optional = false) @safe {
        entryName = name; entryType = type; entryDesc = desc; entryIsOptional = optional;
    }

    /** 
     * Creates definition of the entry for types
     * Returns: String with the entry definition
     */
    public string genDefinition () {
        string result = `` ~
            `    /** ` ~ entryDesc ~ ` */` ~ '\n' ~
            `    private ` ~ entryType ~ ` _` ~ entryName ~ `;` ~ '\n';
        return result;
    }

    /** 
     * Create initialization for constructor
     * Returns: String with initialization of the entry
     */
    public string genInit () {
        string defaultValue = ``;
        switch (entryType) {
            case "string" : defaultValue = `""`;         break;
            case "bool"   : defaultValue = `false`;      break;
            case "ulong"  : defaultValue = `0`;          break;
            case "double" : defaultValue = `double.nan`; break;
            default       : defaultValue = `null`;
        }
        return `        _` ~ entryName ~ ` = ` ~ defaultValue ~ `;` ~ '\n';
    }

    /** 
     * Generate properties for access to the type's entries
     * Returns: String with properties code
     */
    public string genProps () {
        string props = `` ~
            `    /**` ~ '\n' ~
            `     * Getter for '_` ~ entryName ~ `'` ~ '\n' ~
            `     * Returns: Current value of '_` ~ entryName ~ `'` ~ '\n' ~
            `     */` ~ '\n' ~
            `    @property ` ~ entryType ~ ` ` ~ Markup.makePropName(entryName) ~ ` () { return _` ~ entryName ~ `; }` ~ '\n' ~
            `    /**` ~ '\n' ~
            `     * Setter for '_` ~ entryName ~ `'` ~ '\n' ~
            `     * Params: ` ~ Markup.makePropName(entryName) ~ `New = New value of '_` ~ entryName ~ `'` ~ '\n' ~
            `     * Returns: New value of '_` ~ entryName ~ `'` ~ '\n' ~
            `     */` ~ '\n' ~
            `    @property ` ~ entryType ~ ` ` ~ Markup.makePropName(entryName) ~ ` ( ` ~ entryType ~ ` ` ~ Markup.makePropName(entryName) ~ `New ) { return _` ~ entryName ~ ` = ` ~ Markup.makePropName(entryName) ~ `New; }` ~ '\n';
        return props;
    }

    /** 
     * Creates code for setting data from JSON response
     * Returns: String with code for setFromJson
     */
    public string genJsonImport () {
        string jsonImport = ``;
        if (entryIsOptional == false) {
            jsonImport ~= `        if ( "` ~ entryName ~ `" !in data ) throw new TelegramException("Could not find reqired entry : ` ~ entryName ~ `");`;
        } else {
            jsonImport ~= `        if ( "` ~ entryName ~ `" in data )`;
        } jsonImport ~= '\n' ~ `        `;

        switch (entryType) {
            case "string" : jsonImport ~= `_` ~ entryName ~ ` = data["` ~ entryName ~ `"].str();` ~ '\n';      break;
            case "ulong"  : jsonImport ~= `_` ~ entryName ~ ` = data["` ~ entryName ~ `"].integer();` ~ '\n';  break;
            case "bool"   : jsonImport ~= `_` ~ entryName ~ ` = data["` ~ entryName ~ `"].boolean();` ~ '\n';  break;
            case "double" : jsonImport ~= `_` ~ entryName ~ ` = data["` ~ entryName ~ `"].floating();` ~ '\n'; break;
            default       : jsonImport ~= `_` ~ entryName ~ ` = new ` ~ entryType ~ `(data["` ~ entryName ~ `"]);` ~ '\n';
        }

        return jsonImport;
    }

    /** 
     * Generate code for sending as request
     * Returns: String with code for getAsJson
     */
    public string genJsonExport () {
        string jsonExport =``;
        if (entryIsOptional == false || entryType == `bool`) {
            if ([`bool`, `string`, `ulong`, `double`].count(entryType)) {
                jsonExport ~= `        data["` ~ entryName ~ `"] = _` ~ entryName ~ `;` ~ '\n';
            } else {
                jsonExport ~= `        data["` ~ entryName ~ `"] = _` ~ entryName ~ `.getAsJson();` ~ '\n';
            }
        } else {
            switch (entryType) {
                case "string" : jsonExport ~= `        if ( _` ~ entryName ~ ` != "" ) data["` ~ entryName ~ `"] = _` ~ entryName ~ `;` ~ '\n';   break;
                case "double" : jsonExport ~= `        if ( _` ~ entryName ~ ` != double.nan ) data["` ~ entryName ~ `"] = _` ~ entryName ~ `;` ~ '\n';  break;
                case "ulong"  : jsonExport ~= `        if ( _` ~ entryName ~ ` != 0 ) data["` ~ entryName ~ `"] = _` ~ entryName ~ `;` ~ '\n';    break;
                default       : jsonExport ~= `        if ( _` ~ entryName ~ ` !is null ) data["` ~ entryName ~ `"] = _` ~ entryName ~ `.getAsJson();` ~ '\n';
            }
        }

        return jsonExport;
    }
}

/** 
 * Class child form TelegramType
 */
class Type {
    public string typeName;
    public string typeDesc;

    public Entry [] entries;

    /** 
     * Creates new object
     * Params:
     *   name = Name of the type
     *   desc = Description of the type
     *   en = Entries of the type
     */
    public this (string name, string desc, Entry [] en) @safe {
        typeName = name; typeDesc = desc; entries = en;
    }

    /** 
     * Generate new telegram type
     * Returns: String with type definition
     */
    public string genDefinition () {
        string definition = `` ~
            `/**` ~ '\n' ~
            ` * ` ~ typeDesc ~ '\n' ~
            ` */` ~ '\n' ~
            `class ` ~ typeName ~ ` : TelegramType {` ~ '\n';

        definition ~= genConstructor ();
        definition ~= genImport();
        definition ~= genExport();
        definition ~= genEntries();

        definition ~= `}` ~ '\n';
        return definition;
    }

    /** 
     * Generate consttuctor for the type
     * Returns: String with constructor code
     */
    private string genConstructor () {
        string constructor = `` ~
            `    /**` ~ '\n' ~
            `     * Creates new type object` ~ '\n' ~
            `     */` ~ '\n' ~
            `    nothrow pure public this () @safe {` ~ '\n';

        foreach (en; entries) {
            constructor ~= en.genInit ();
        }

        constructor ~= `    }` ~ '\n' ~
            '\n' ~
            `    /** Add constructor with data init from response */` ~ '\n' ~
            `    mixin(TelegramTypeConstructor);` ~ '\n' ~
            '\n';

        return constructor;
    }

    /** 
     * Generates setFromJson
     * Returns: Generated code
     */
    private string genImport () {
        string importJson = `` ~
            `    override public void setFromJson (JSONValue data) {` ~ '\n';

        foreach (i, en; entries) {
            importJson ~= en.genJsonImport ();
            if (i + 1 != entries.length) importJson ~= '\n'; 
        }

        importJson ~= `    }` ~ '\n' ~ '\n';
        return importJson;
    }

    /** 
     * Generates getAsJson
     * Returns: Generated code
     */
    private string genExport () {
        string exportJson = `` ~
            `    override public JSONValue getAsJson () {` ~ '\n' ~
            `        JSONValue data = parseJSON("");` ~ '\n' ~ '\n';

        foreach (i, en; entries) {
            exportJson ~= en.genJsonExport ();
            exportJson ~= '\n'; 
        }

        exportJson ~= `` ~
            `        return data;` ~ '\n' ~
            `    }` ~ '\n' ~ '\n';
        return exportJson;
    }

    /** 
     * Generate definitions and properties
     * Returns: Generated code
     */
    private string genEntries () {
        string entiesCode = ``;

        foreach (i, en; entries) {
            entiesCode ~= en.genDefinition () ~ en.genProps ();
            if (i + 1 != entries.length) entiesCode ~= '\n';
        }

        return entiesCode;
    }

    /** 
     * Creates file with current type
     * Params:
     *   moduleName = Name of module for generation
     * Returns: Generated code
     */
    public string genFile (string moduleName) {
        string typeFile = `` ~ 
        `/**` ~ '\n' ~
        ` * Contains ` ~ typeName ~ '\n' ~
        ` */` ~ '\n' ~
        `module ` ~ moduleName ~ `;` ~ '\n' ~
        '\n' ~
        `import tg.core.type, tg.core.exception;` ~ '\n' ~
        `import std.json, tg.type;` ~ '\n' ~
        '\n' ~
        genDefinition() ~ '\n';

        return typeFile;
    }
}
