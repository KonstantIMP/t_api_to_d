/**
 * Class for downloading, parsing and printing telegram types
 */
module kimp.parser;

import requests : getContent;
import arsd.dom, std.conv : to;
import std.stdio, kimp.type;
import std.algorithm : count;
import std.ascii : isUpper;
import std.string : toLower;
import std.string : replace;
import kimp.markup, std.file;

/** 
 * Class for downloading and parsing TAPI
 */
class TApiParser {
    private Document htmlTApi; 
    private Type[][string] types;

    /** 
     * Inits the object and dowbload API
     */
    public this () {
        htmlTApi = new Document(to!string(getContent("https://core.telegram.org/bots/api")));
    }

    /** 
     * Parse downloaded api
     */
    public void parse () {
        auto devPageContent = htmlTApi.getElementById("dev_page_content").children;
        string currentSection = "";

        for (ulong i = 0; i < devPageContent.length; i++) {
            if (devPageContent[i].tagName == `h3`) {
                writeln (`[DEBUG] Found new section : `, devPageContent[i].children[1].toString());
                currentSection = devPageContent[i].children[1].toString();

                if (["Recent changes", "Authorizing your bot", "Making requests", "Using a Local Bot API Server"].count(currentSection)) {
                    writeln (`[DEBUG] This section is useless : skip`); i++;
                    while (devPageContent[i].tagName != `h3`) i++;
                    i = i - 1; continue;
                }
            } else if (devPageContent[i].tagName == `h4`) {
                writeln (`[DEBUG] Found H4 tag : analyze`);
                if (isUpper(devPageContent[i].children[1].toString()[0])) {
                    string typeName = devPageContent[i].children[1].toString();
                    writeln (`[DEBUG] Found a type : `, typeName); i += 2;

                    if (devPageContent[i].tagName != `p`) {
                        writeln (`[DEBUG] Type without description : scip`);
                        i -= 2; continue;
                    }

                    string typeDesc = ``;
                    foreach (child; devPageContent[i].children) {
                        if (child.tagName == `a`) typeDesc ~= child.children[0].toString();
                        else typeDesc ~= child.toString();
                    } i += 2;
                    writeln (`[DEBUG] Found a type desc : `, typeDesc);

                    Entry [] entries = new Entry[0];
                    if (devPageContent[i].tagName == `table`) {
                        foreach (tr; devPageContent[i].children[3].children) {
                            if (tr.tagName == `tr`) {
                                string entryName = tr.children[1].children[0].toString();
                                writeln (`[DEBUG] Found entry for `, typeName, ` : `, entryName);

                                string entryType = ``;
                                foreach (t; tr.children[3].children) {
                                    if (t.tagName == "a") entryType ~= t.children[0].toString();
                                    else entryType ~= t.toString();
                                }
                                writeln (`[DEBUG] Type for this entry : `, entryType);

                                string entryDesc = ``;
                                foreach (t; tr.children[5].children) {
                                    if (t.tagName == "a") entryDesc ~= t.children[0].toString();
                                    else entryDesc ~= t.toString();
                                }
                                writeln (`[DEBUG] Desc for this entry : `, entryDesc);

                                string typePostFix = "";
                                if (entryType.toLower().count("array of") != 0 || entryType.toLower().count(" or ")) {
                                    if (entryType.toLower().count("array of") == 2) {
                                        typePostFix = "[][]";
                                        entryType = entryType.replace("Array of Array of ", "");
                                    }
                                    else if (entryType.toLower().count("array of") == 1) {
                                        typePostFix = "[]";
                                        entryType = entryType.replace("Array of ", "");
                                    }
                                    else {
                                        entryType = "String";
                                    }
                                }

                                if (entryType == "String") entryType = "string";
                                else if (entryType == "Integer") entryType = "ulong";
                                else if (entryType == "Boolean" || entryType == "True") entryType = "bool";
                                else if (entryType == "Float number" || entryType == "Float") entryType = "double";
                                else entryType = "Telegram" ~ entryType;

                                entries.length += 1; entries[$ - 1] = new Entry(entryName, entryType ~ typePostFix, entryDesc, entryDesc.toLower().count(`optional`) > 0);
                            }
                        }
                    } else {
                        writeln (`[DEBUG] Type without entries : `, typeName, ` : skip`);
                        i -= 4; continue;
                    }

                    types[currentSection] ~= new Type ("Telegram" ~ typeName, typeDesc, entries);
                } else {
                    writeln (`[DEBUG] Incorrect name : it may be method`);
                }
            }
        } addUnparsed ();
    }

    /** 
     * Add unparsed types to the array
     */
    private void addUnparsed () {
        types["Games"] ~= new Type("TelegramCallbackGame", "A placeholder, currently holds no information. Use BotFather to set up your game.", []);
    
        types["Available types"] ~= new Type("TelegramVoiceChatStarted", "This object represents a service message about a voice chat started in the chat. Currently holds no information", []);
        types["Available types"] ~= new Type("TelegramLoginUrl", "This object represents a parameter of the inline keyboard button used to automatically authorize a user.", [
            new Entry("url", "string", "An HTTP URL to be opened with user authorization data added to the query string when the button is pressed", false),
            new Entry("forward_text", "string", "Optional. New text of the button in forwarded messages.", true),
            new Entry("bot_username", "string", "Optional. Username of a bot, which will be used for user authorization", true),
            new Entry("request_write_access", "bool", "Optional. Pass True to request the permission for your bot to send messages to the user.", true)
        ]);
        types["Available types"] ~= new Type("TelegramFile", "This object represents a file ready to be downloaded", [
            new Entry("file_id", "string", "Identifier for this file, which can be used to download or reuse the file", false),
            new Entry("file_unique_id", "string", "Unique identifier for this file, which is supposed to be the same over time and for different bots", false),
            new Entry("file_size", "ulong", "File size, if known", true),
            new Entry("file_path", "string", "File path", true)
        ]);
    }

    /** 
     * Print info about parsed types
     */
    public void printInfo () {
        writeln ("[INFO] Parsed data types");
        foreach (section; types.byKey) {
            foreach (t; types[section]) {
                writeln ('\t', section, " : ", t.typeName);
            }
        }
    }

    /** 
     * Create files with parsed data
     */
    public void createOut () {
        if (exists("out/")) rmdirRecurse("out/");
        createOutTree ();

        string tgType = `` ~
            `/**` ~ '\n' ~
            ` * Full pack of Telegram types` ~ '\n' ~
            ` */` ~ '\n' ~
            `module tg.type;` ~ '\n' ~
            '\n' ~
            `public import tg.core.type;` ~ '\n' ~
            '\n';

        foreach (section; types.byKey()) {
            string filePath, importPath;
            if (section == "Games") {
                filePath ~= "out/tg/game/types/";
                importPath ~= "tg.game.types.";
            }
            else if (section == "Inline mode") {
                filePath ~= "out/tg/inline/types/";
                importPath ~= "tg.inline.types.";
            }
            else if (section == "Getting updates") {
                filePath ~= "out/tg/update/types/";
                importPath ~= "tg.update.types.";
            }
            else if (section == "Stickers") {
                filePath ~= "out/tg/sticker/types/";
                importPath ~= "tg.sticker.types.";
            }
            else if (section == "Telegram Passport") {
                filePath ~= "out/tg/passport/types/";
                importPath ~= "tg.passport.types.";
            }
            else if (section == "Payments") {
                filePath ~= "out/tg/payment/types/";
                importPath ~= "tg.payment.types.";
            }
            else {
                filePath ~= "out/tg/types/";
                importPath ~= "tg.types.";
            }

            foreach (t; types[section]) {
                tgType ~= `public import ` ~ importPath ~ Markup.makeFileName(t.typeName) ~ `;` ~ '\n';
                std.file.write (filePath ~ Markup.makeFileName(t.typeName) ~ ".d", t.genFile (importPath ~ Markup.makeFileName(t.typeName)));
            } tgType ~= '\n';
        } 
        tgType ~= addAliases ();  
        std.file.write ("out/tg/type.d", tgType);

        copy ("resource/prebuilt/exception.d", "out/tg/core/exception.d");
        copy ("resource/prebuilt/type.d", "out/tg/core/type.d");
    }

    /** 
     * Generate aliases for some types
     * Returns: String with type aliases
     */
    private string addAliases () {
        string aliases = `` ~ 
            `alias TelegramChatMember = TelegramVariant;` ~ '\n' ~
            `alias TelegramInputMessageContent = TelegramVariant;` ~ '\n' ~
            '\n';
        return aliases;
    }

    /** 
     * Creates folder in the output
     */
    private void createOutTree () {
        mkdirRecurse ("out/tg/");
        mkdirRecurse ("out/tg/types");
        mkdirRecurse ("out/tg/core");
        mkdirRecurse ("out/tg/update/types");
        mkdirRecurse ("out/tg/sticker/types");
        mkdirRecurse ("out/tg/passport/types");
        mkdirRecurse ("out/tg/payment/types");
        mkdirRecurse ("out/tg/game/types");
        mkdirRecurse ("out/tg/inline/types");
    }
}
